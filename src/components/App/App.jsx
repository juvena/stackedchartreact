import React from 'react'

import './App.css'
import Chart from '../Chart/Chart'

export default () =>
  <div className="App">
    <div className="App-chart-container">
      <Chart/>
    </div>

  </div>
